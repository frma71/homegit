alias mondual='xrandr --output eDP1 --mode 3200x1800 --below HDMI1 --output HDMI1 --mode 2560x1440 --primary --output eDP1 --rotate inverted'
alias monmirror='xrandr --output eDP1 --mode 3200x1800 --same-as HDMI1 --output HDMI1 --mode 2560x1440'
alias moninternal='xrandr --output eDP1 --mode 3200x1800 --output HDMI1 --off --output eDP1 --rotate normal --primary'
alias monexternal='xrandr --output HDMI1 --mode 2560x1440 --output eDP1 --off --primary'
