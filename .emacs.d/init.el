(message "init.el running")
(require 'package)
(package-initialize)
(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/") t)

(add-to-list 'load-path "~/.emacs.d/misc")

;; after copy Ctrl+c in Linux X11, you can paste by `yank' in emacs
(setq x-select-enable-clipboard t)
;; after mouse selection in X11, you can paste by `yank' in emacs
(setq x-select-enable-primary t)

(setq-default show-trailing-whitespace t)

; openscad
(autoload 'scad-mode "custom/scad-mode" "Major mode for editing SCAD code." t)
(add-to-list 'auto-mode-alist '("\\.scad$" . scad-mode))

; No tool bar or menu bar
(tool-bar-mode -1)
(menu-bar-mode -1)

(load-theme 'zenburn t)

(add-hook 'c-mode-common-hook
	  (lambda ()
	    (when (derived-mode-p 'c-mode 'c++-mode 'java-mode)
	      (ggtags-mode 1))))

(setq c-default-style "linux")

(message "init.el done")
